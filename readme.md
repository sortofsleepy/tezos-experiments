Tezos Experiments
====

Early experiments messing around with smart contracts. This uses [Smartpy](https://smartpy.io)'s wrapper library around the Michelson language 
which is what Tezos uses for writing smart contracts.

This focuses on using the Python version of the language; there is a Typescript version but personally I'm not a fan of some of 
the rules they've set up in how they compiled their cli library and I"m not sure how to fix without rebuilding things myself, 
so we're sticking with Python. 

Setup 
=== 
* This includes a instance of the cli library that's needed with an added `__init__.py` file; it doesn't really serve a purpose other
than to turn the cli library into a module and to make it more compatible with IDEs.

* If you need to re-install, you can run `scripts/install.sh`
* `scripts/run.sh` will run the compilation functionality of the cli
* `scripts/deploy.sh` will run the origination functionality of the cli
* `scripts/test.sh` will run the testing functionality of the cli
 