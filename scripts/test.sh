#!/usr/bin/env bash

# Runs any tests specified in the contract.

usage () {
    >&2 cat <<EOF
Usage: $(basename $0) <options>

The options are as follows:

--file
  path to contract file.

--output
  path of where to output everything.

EOF
exit
}

while [[ $# -gt 0 ]]; do
    case "$1" in
        --file)
            file="$2"
            shift 2
            ;;
        --output)
            output="$2"
            shift 2
            ;;

          --help)
            usage
            ;;
          --h)
            usage
            ;;
        *)
            >&2 echo Unexpected argument: "$1"
            exit 1
            ;;
    esac
done

./smartpy/SmartPy.sh test $file $output
