#!/usr/bin/env bash

# Deploys the contract

usage () {
    >&2 cat <<EOF
Usage: $(basename $0) <options>

The options are as follows:

--contract <path to file>
        Path to the contract file.

--storage <path to file>
        Path to storage file.

--rpc <node url>
    URL to where the contract will be deployed.

EOF
exit
}

while [[ $# -gt 0 ]]; do
    case "$1" in
        --contract)
            contract="$2"
            shift 2
            ;;
        --storage)
            storage="$2"
            shift 2
            ;;
        --rpc)
            rpc="$2"
            shift 2
            ;;
          --help)
            usage
            ;;
          --h)
            usage
            ;;
        *)
            >&2 echo Unexpected argument: "$1"
            exit 1
            ;;
    esac
done

./smartpy/SmartPy.sh originate-contract --code $contract --storage $storage --rpc $rpc;
