import smartpy as sp

# load template
FA2 = sp.io.import_script_from_url("https://smartpy.io/templates/fa2_lib.py")

# setup metadata
meta = sp.big_map({"content": sp.utils.bytes_of_string("testing coin")})

# setup admin address
admin_address = sp.address("tz1gDzUHhTqncbjwi7vWZsMvQMgALcDV8iwH")

# just a dumb test contract
class TestCoin(FA2.Admin, FA2.Fa2Nft):
    def __init__(self, admin, paused=False, **kwargs):
        FA2.Fa2Nft.__init__(self, **kwargs)
        FA2.Admin.__init__(self, admin)

        # add some additional elements to the contract's data.
        self.update_initial_storage(
            closed=False,
            paused=paused,
            supply=500,
            token_metadata=sp.big_map(
                {},
                tkey=sp.TNat,
                tvalue=sp.TRecord(
                    token_id=sp.TNat,
                    token_info=sp.TMap(sp.TString, sp.TBytes)
                ).layout(("token_id", "token_info")),
            )
        )

    @sp.entry_point
    def mint(self, params):
        # ensure caller is admin.
        sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')

        # ensure supply is adequate
        sp.verify(self.data.supply > 0, message="There are not enough tokens available.")

        with sp.for_("action", params) as action:
            # verify that user wants at least one token.
            sp.verify((action.amount > 0))

            token_id = sp.compute(self.data.last_token_id)
            metadata = sp.record(token_id=token_id, token_info=action.metadata)

            # set token data.
            self.data.token_metadata[token_id] = metadata
            self.data.ledger[token_id] = action.to_

            # increment token id
            self.data.last_token_id += 1

            # remove one token from supply
            self.data.supply -= 1;


sp.add_compilation_target("compilation", TestCoin(
    admin=admin_address,
    metadata=meta,
    paused=True
))


# adding the decorator appears to scaffold everything into the necessary functions - see the "Code" section after running.
@sp.add_test(name="tests")
def test():
    jerry = sp.test_account("Jerry")
    tom = sp.test_account("Tom")
    admin = sp.address("tz1gDzUHhTqncbjwi7vWZsMvQMgALcDV8iwH")

    # start sceneario
    scenario = sp.test_scenario()
    scenario.h1("TESTING")

    nft = TestCoin(admin=admin, metadata=sp.big_map({"content": sp.utils.bytes_of_string("testing coin")}))
    scenario += nft
    nft.mint([sp.record(amount=sp.int(1), to_=jerry.address, metadata={
        "poop":sp.utils.bytes_of_string("testing coin"),
        "poop2":sp.utils.bytes_of_string("testing coin2"),
        "poop3":sp.utils.bytes_of_string("testing coin3"),

    })]).run(sender=admin)

# https://ghostnet.smartpy.io
