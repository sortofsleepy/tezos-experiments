import smartpy as sp

#FA2 = sp.io.import_script_from_url("https://smartpy.io/templates/fa2_lib.py")
FA2 = sp.io.import_script_from_url("https://smartpy.io/templates/FA2.py")


meta = sp.big_map({"content": sp.utils.bytes_of_string("testing coin")})

admin_address = sp.address("tz1gDzUHhTqncbjwi7vWZsMvQMgALcDV8iwH")


class TestCoin(FA2.FA2):
    pass



sp.add_compilation_target("compilation", TestCoin(FA2.FA2_config(non_fungible=True), admin=admin_address, metadata = sp.big_map({"": sp.utils.bytes_of_string("tezos-storage:content"),"content": sp.utils.bytes_of_string("""{"name": "Tutorial Contract", "description": "NFT contract for the tutorial"}""")})))

# https://ghostnet.smartpy.io
